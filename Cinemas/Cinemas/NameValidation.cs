﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;
using System.Reflection;

namespace Cinemas
{
    public class NameValidation : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value,
                            ValidationContext validationContext)
        {
            bool flag = false;
            Regex regex = new Regex(@"^[a-zA-Z]+$");

            if (value != null)
            {

                if (regex.IsMatch(value.ToString()))
                {
                    flag = true;
                }

                if (flag)
                {
                    return null;
                }
                else
                {
                    ValidationResult result = new ValidationResult
                        ("Special Characters are not allowed!");
                    return result;
                }
            }
            else
            {
                ValidationResult result = new ValidationResult
                        ("Field is required!!");
                return result;
            }

            
        }

        
    
}
}




        



