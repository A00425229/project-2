﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;
using System.Reflection;

namespace Cinemas
{
    public class CCExpirtDateValidate : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value,
                            ValidationContext validationContext)

        {
            if (value != null)

            {
                string str = value.ToString();

                if (!IsExpDate(str))
                {
                    return new ValidationResult("Invalid date. Insert numbers.");
                }

                String sDate = DateTime.Now.ToString();
                DateTime datevalue = (Convert.ToDateTime(sDate.ToString()));

                int mn = Convert.ToInt32(datevalue.Month.ToString());
                int yy = Convert.ToInt32(datevalue.Year.ToString().Substring(2,2));

                int ExpM = Convert.ToInt32(str.Substring(0, 2));
                int ExpY = Convert.ToInt32(str.Substring(3, 2));

                if (ExpY > yy )
                { return ValidationResult.Success; }
                else if (ExpY == yy)
                {
                    if (ExpM >= mn)

                    {
                         return ValidationResult.Success; }
                    else
                    {
                        return new ValidationResult("Card is expired"); }
                }
                else
                {
                    return new ValidationResult("Card is expired"); }

            }

            else
            {
                return new ValidationResult("Expiry Date is required");
            }
        }
         
        private bool IsExpDate(string postal)
            {
                bool IsDate = false;
            string pattern = @"(0[1-9]|1[0-2])/([0-9][0-9])$";
                Regex regex = new Regex(pattern);
                return IsDate = regex.IsMatch(postal);
            }

        
    }
}








