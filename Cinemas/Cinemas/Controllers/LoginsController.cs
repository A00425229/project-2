﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Cinemas.Models;

namespace Cinemas.Controllers
{
    public class LoginsController : Controller
    {
        private LuxeCinemasEntities db = new LuxeCinemasEntities();

        // GET: Logins
        public ActionResult Index()
        {
            try
            {
                //var logins = db.Logins.Include(l => l.Customer);
                //return View(logins.ToList());
                return View();
            }
            catch (Exception ex)
            {
                return View("~/Views/Shared/Error.cshtml");
            }
        }

        // GET: Logins/Details/5
        public ActionResult Details()
        {
            try
            {
                var logins = db.Logins.Include(l => l.Customer);
                return View(logins.ToList());
            }
            catch (Exception ex)
            {
                return View("~/Views/Shared/Error.cshtml");
            }
        }

        // GET: Logins/Create
        public ActionResult Create()
        {
            try
            {
                ViewBag.CustomerId = new SelectList(db.Customers, "ID", "firstName");
                return View();
            }
            catch (Exception ex)
            {
                return View("~/Views/Shared/Error.cshtml");
            }
        }


        // POST: Logins/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,CustomerId,loginId,password,IsAdmin")] Login login)
        {
            try
            {


                if (ModelState.IsValid)
                {
                    db.Logins.Add(login);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }

                ViewBag.CustomerId = new SelectList(db.Customers, "ID", "firstName", login.CustomerId);
                return View(login);
            }
            catch (Exception ex)
            {
                return View("~/Views/Shared/Error.cshtml");
            }
        }



        [HttpPost]
        public ActionResult Validate(Login login)
        {
            try
            {
                //var result = db1.Logins.Where(cls => cls.loginId.Equals(login.ID) && cls.password.Equals(login.password)).Any();
                string loginName = login.loginId;
                string pwd = login.password;
                var record = db.Logins.Where(u => u.loginId == loginName && u.password == pwd).FirstOrDefault();

                if (record != null && !string.IsNullOrEmpty(Convert.ToString(record.CustomerId)))
                {
                    Session["CustId"] = Convert.ToString(record.CustomerId);
                    return RedirectToAction("Create", "Bookings");

                }
                else
                {
                    return View("~/Views/Shared/LoginFailed.cshtml");
                }
            }


            catch (Exception ex)
            {
                return View("~/Views/Shared/Error.cshtml");
            }


        }


        // GET: Logins/Edit/5
        public ActionResult Edit(int? id)
        {
            try
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                Login login = db.Logins.Find(id);
                if (login == null)
                {
                    return HttpNotFound();
                }
                ViewBag.CustomerId = new SelectList(db.Customers, "ID", "firstName", login.CustomerId);
                return View(login);
            }
            catch (Exception ex)
            {
                return View("~/Views/Shared/Error.cshtml");
            }
        }

        // POST: Logins/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,CustomerId,loginId,password,IsAdmin")] Login login)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    db.Entry(login).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                ViewBag.CustomerId = new SelectList(db.Customers, "ID", "firstName", login.CustomerId);
                return View(login);
            }

            catch (Exception ex)
            {
                return View("~/Views/Shared/Error.cshtml");
            }
        }

        // GET: Logins/Delete/5
        public ActionResult Delete(int? id)
        {
            try
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                Login login = db.Logins.Find(id);
                if (login == null)
                {
                    return HttpNotFound();
                }
                return View(login);
            }
            catch (Exception ex)
            {
                return View("~/Views/Shared/Error.cshtml");
            }
        }

        // POST: Logins/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            try
            {
                Login login = db.Logins.Find(id);
                db.Logins.Remove(login);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                return View("~/Views/Shared/Error.cshtml");
            }
        }

        protected override void Dispose(bool disposing)
        {

            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);


        }
    }
}
