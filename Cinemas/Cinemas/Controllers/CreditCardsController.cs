﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Cinemas.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Cinemas.Controllers
{
    public class CreditCardsController : Controller
    {
        private LuxeCinemasEntities db = new LuxeCinemasEntities();

        // GET: CreditCards
        public async Task<ActionResult> Index()
        {
            List<CreditCard> CreditCardInfo = new List<CreditCard>();
            using (var client = new HttpClient())
            {
                //Passing service base url 
                client.BaseAddress = new Uri("http://localhost:8080/MovieBookingRest/rest/");
                client.DefaultRequestHeaders.Clear();
                //Define request data format 
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                //Sending request to find web api REST service resource GetAllEmployees using HttpClient 
                HttpResponseMessage Res = await client.GetAsync("CreditCard");
                //Checking the response is successful or not which is sent using HttpClient 
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api  
                    var CreditCardResponse = Res.Content.ReadAsStringAsync().Result;
                    //Deserializing the response recieved from web api and storing into the Employee list 
                    CreditCardInfo = JsonConvert.DeserializeObject<List<CreditCard>>(CreditCardResponse);
                }
                //returning the employee list to view 
                return View(CreditCardInfo);
            }
        }

        // GET: CreditCards/Details/5
        public ActionResult Details(int? id)
        {
            try
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                CreditCard creditCard = db.CreditCards.Find(id);
                if (creditCard == null)
                {
                    return HttpNotFound();
                }
                return View(creditCard);
            }
            catch (Exception ex)
            {
                return View("~/Views/Shared/Error.cshtml");
            }
        }

        // GET: CreditCards/Create
        public ActionResult Create()
        {
            try
            {
                ViewBag.CustomerId = new SelectList(db.Customers, "ID", "firstName");
                return View();
            }
            catch (Exception ex)
            {
                return View("~/Views/Shared/Error.cshtml");
            }
        }

        // POST: CreditCards/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Create([Bind(Include = "ID,CustomerId,CardType,CardNumber,SecurityPin,ExpiryDate")] CreditCard creditCard)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.CreditCards.Add(creditCard);
        //        db.SaveChanges();
        //        return RedirectToAction("Index");
        //    }

        //    ViewBag.CustomerId = new SelectList(db.Customers, "ID", "firstName", creditCard.CustomerId);
        //    return View(creditCard);
        //}

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(CreditCard creditObj)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    CreditCard cardObj = new CreditCard();

                    cardObj.CardNumber = creditObj.CardNumber;
                    cardObj.CardType = creditObj.CardType;
                    cardObj.CustomerId = Convert.ToInt32(Session["CustId"]);                    
                    cardObj.NameOnCard = creditObj.NameOnCard;
                    cardObj.SecurityPin = creditObj.SecurityPin;
                    cardObj.ExpiryDate = creditObj.ExpiryDate;
                    
                    string addcustomerMethod = "CreditCard";
                    String Baseurl = "http://localhost:8080/MovieBookingRest/rest/";
                    HttpResponseMessage responseMessage = await client.PostAsJsonAsync(Baseurl + addcustomerMethod, cardObj);
                    Console.WriteLine(cardObj);
                    if (responseMessage.IsSuccessStatusCode)
                    {
                        return RedirectToAction("Create", "Bookings");
                    }
                    else
                    {
                        return View("~/Views/Shared/Error.cshtml");
                    }
                }
            }

            catch (Exception ex)
            {
                return View("~/Views/Shared/Error.cshtml");
            }
        }
           


        // GET: CreditCards/Edit/5
        public ActionResult Edit(int? id)
        {
            try
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                CreditCard creditCard = db.CreditCards.Find(id);
                if (creditCard == null)
                {
                    return HttpNotFound();
                }
                ViewBag.CustomerId = new SelectList(db.Customers, "ID", "firstName", creditCard.CustomerId);
                return View(creditCard);
            }
            catch (Exception ex)
            {
                return View("~/Views/Shared/Error.cshtml");
            }
        }

        // POST: CreditCards/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,CustomerId,CardType,CardNumber,SecurityPin,ExpiryDate")] CreditCard creditCard)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    db.Entry(creditCard).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                ViewBag.CustomerId = new SelectList(db.Customers, "ID", "firstName", creditCard.CustomerId);
                return View(creditCard);
            }
            catch (Exception ex)
            {
                return View("~/Views/Shared/Error.cshtml");
            }
        }

        // GET: CreditCards/Delete/5
        public ActionResult Delete(int? id)
        {
            try
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                CreditCard creditCard = db.CreditCards.Find(id);
                if (creditCard == null)
                {
                    return HttpNotFound();
                }
                return View(creditCard);
            }
            catch (Exception ex)
            {
                return View("~/Views/Shared/Error.cshtml");
            }
        }

        // POST: CreditCards/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            try
            {
                CreditCard creditCard = db.CreditCards.Find(id);
                db.CreditCards.Remove(creditCard);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                return View("~/Views/Shared/Error.cshtml");
            }

        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

       
    }
}
