﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;
using System.Reflection;

namespace Cinemas
{
    public class DateValidation : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value,
                            ValidationContext validationContext)
        {
            if (value != null)
            {
                DateTime bookingDate = Convert.ToDateTime(value.ToString());
                if (bookingDate < DateTime.Now)
                {
                    ValidationResult result = new ValidationResult
                        ("Select a Movie Date in Future");
                    return result;
                }
                else if (bookingDate > DateTime.Now.AddDays(7))
                {
                    ValidationResult result = new ValidationResult
                        ("Please select a date which is not greater than a week!");
                    return result;
                }
                else
                {
                    return null;
                }
            }
            else
            {
                ValidationResult result = new ValidationResult
                        ("Field is required!!");
                return result;
            }

        }



    }
}




        



